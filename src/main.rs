use std::string::FromUtf8Error;

use rand::Rng;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Use symbols in password
    #[arg(short, long, default_value_t = false)]
    use_symbols: bool,

    /// Password size
    #[arg(short, long, default_value_t = 8)]
    size: usize,
}


fn gen_password(size: usize, has_symbols: bool) -> Result<String, FromUtf8Error> {
    let symbols: Vec<u8> = (33..=47)
        .chain(58..=64)
        .chain(91..=96)
        .chain(123..=126)
        .collect();
    let mut random_thread = rand::thread_rng();
    let mut result: Vec<u8> = vec![];

    for _ in 0..size {
        let random_num: u8 = if has_symbols {
            random_thread.gen_range(33..=126)
        } else {
            let mut res = random_thread.gen_range(33..=126);
            
            while symbols.iter().any(|&x| x == res) {
                res = random_thread.gen_range(33..=126);
            }

            res
        };

        result.push(random_num);
    }

    String::from_utf8(result)
}

fn main() {
    let args = Args::parse();
    let use_symbols = args.use_symbols;
    let size = args.size;

    println!("{}", gen_password(size, use_symbols).unwrap());
}
